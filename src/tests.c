#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

static void print_test_start(int test_id) {
  fprintf(stdout, "=== TEST #%i: STARTING ===\n", test_id);
}

static void print_test_failed_message(int test_id) {
  fprintf(stderr, "=== TEST #%i: FAILED ===\n", test_id);
}

static void print_test_passed_message(int test_id) {
  fprintf(stdout, "=== TEST #%i: PASSED ===\n", test_id);
}

static inline void destroy_heap(void* heap, size_t size) {
  munmap(heap, size_from_capacity((block_capacity) {.bytes = size}).bytes);
}

static inline struct block_header* find_header(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

/**
 * Обычное успешное выделение памяти
 */
void run_first_test(void) {
  int test_id = 1;
  print_test_start(test_id);

  void* heap = heap_init(8192);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* allocation = _malloc(4096);
  printf("Heap after malloc:\n");
  debug_heap(stdout, heap);
  if (!heap || !allocation) {
    print_test_failed_message(test_id);
    return;
  }

  _free(allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 8192);
  print_test_passed_message(test_id);
}

/**
 * Освобождение одного блока из нескольких выделенных
 */
void run_second_test(void) {
  int test_id = 2;
  print_test_start(test_id);

  void* heap = heap_init(8192);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* first_allocation = _malloc(128);
  void* second_allocation = _malloc(64);
  printf("Heap after mallocs:\n");
  debug_heap(stdout, heap);
  if (!heap || !first_allocation || !second_allocation) {
    print_test_failed_message(test_id);
    return;
  }

  _free(first_allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 8192);
  print_test_passed_message(test_id);
}

/**
 * Освобождение двух блоков из нескольких выделенных
 */
void run_third_test(void) {
  int test_id = 3;
  print_test_start(test_id);

  void* heap = heap_init(8192);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* first_allocation = _malloc(128);
  void* second_allocation = _malloc(64);
  void* third_allocation = _malloc(64);
  printf("Heap after mallocs:\n");
  debug_heap(stdout, heap);
  if (!heap || !first_allocation || !second_allocation || !third_allocation) {
    print_test_failed_message(test_id);
    return;
  }

  _free(first_allocation);
  _free(second_allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 8192);
  print_test_passed_message(test_id);
}

/**
 * Память закончилась, новый регион памяти расширяет старый
 */
void run_fourth_test(void) {
  int test_id = 4;
  print_test_start(test_id);

  void* heap = heap_init(1);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* first_allocation = _malloc(16384);
  void* second_allocation = _malloc(16384);
  printf("Heap after mallocs:\n");
  debug_heap(stdout, heap);
  struct block_header* first_header = find_header(first_allocation);
  if (!heap || !first_allocation || !second_allocation
     || first_header->next != find_header(second_allocation)) {
    print_test_failed_message(test_id);
    return;
  }

  _free(first_allocation);
  _free(second_allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 16384);
  print_test_passed_message(test_id);
}

/**
 * Память закончилась, старый регион памяти не расширить из-за другого выделенного
 * диапазона адресов, новый регион выделяется в другом месте
 */
void run_fifth_test(void) {
  int test_id = 5;
  print_test_start(test_id);

  void* heap = heap_init(1);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* first_allocation = _malloc(16384);
  struct block_header* first_header = find_header(first_allocation);
  if (!heap || !first_allocation) {
    print_test_failed_message(test_id);
    return;
  }

  (void) mmap(first_header->contents + first_header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

  void* second_allocation = _malloc(16384);
  printf("Heap after mallocs:\n");
  debug_heap(stdout, heap);
  if (!second_allocation || find_header(second_allocation) == (void*) (first_header->contents + first_header->capacity.bytes)) {
    print_test_failed_message(test_id);
    return;
  }

  _free(first_allocation);
  _free(second_allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 16384);
  print_test_passed_message(test_id);
}
