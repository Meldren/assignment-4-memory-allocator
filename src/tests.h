#ifndef _TESTS_H_
#define _TESTS_H_

void run_first_test(void);
void run_second_test(void);
void run_third_test(void);
void run_fourth_test(void);
void run_fifth_test(void);

#endif
