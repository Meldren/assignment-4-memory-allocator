#include "tests.h"

int main() {
  run_first_test();
  run_second_test();
  run_third_test();
  run_fourth_test();
  run_fifth_test();
  return 0;
}
